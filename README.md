# README #

 Simple Multi-Layer Neural Network

### What is this repository for? ###

* An example of a neural network implementation.
* This neural network is a feed forward network that learns through back propagation and gradient descent.
* This neural network uses the Sigmoid function and squared error. It also uses momentum to help converge.
* The example trains and tests an XOR function as a proof of concept. (It doesn't have to use so many layers, a single hidden layer with 10 neurons works well)

### How do I get set up? ###

* Project is a Visual studio windows form application.

![img.png](https://bitbucket.org/repo/A55pjj/images/46546001-img.png)