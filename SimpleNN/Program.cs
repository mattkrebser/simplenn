﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleNN
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //make layers
            Layer input = new Layer(2);
            Layer hidden = new Layer(200);
            Layer hidden2 = new Layer(100);
            Layer hidden3 = new Layer(50);
            Layer hidden4 = new Layer(25);
            Layer output = new Layer(1);
            List<Layer> layers = new List<Layer>();
            layers.Add(input);
            layers.Add(hidden);
            layers.Add(hidden2);
            layers.Add(hidden3);
            layers.Add(hidden4);
            layers.Add(output);

            //specify training data
            List<double[]> input_train = new List<double[]>();
            input_train.Add(new double[] { 0, 0 });
            input_train.Add(new double[] { 0, 1 });
            input_train.Add(new double[] { 1, 0 });
            input_train.Add(new double[] { 1, 1 });
            List<double[]> output_train = new List<double[]>();
            output_train.Add(new double[] { 0 });
            output_train.Add(new double[] { 1 });
            output_train.Add(new double[] { 1 });
            output_train.Add(new double[] { 0 });

            //train neural net
            var net = new NNet(layers, 0.4, 0.9, 10000, input_train, output_train,
                Sigmoid, SigmoidDerived, SquaredError, SquaredErrorDerived);

            PrintArray(net.Sample(new double[] { 0, 0 }));
            PrintArray(net.Sample(new double[] { 0, 1 }));
            PrintArray(net.Sample(new double[] { 1, 0 }));
            PrintArray(net.Sample(new double[] { 1, 1 }));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        static double Sigmoid(double x)
        {
            return (x < -45.0 ? (0.0) : (x > 45.0 ? (1.0) : (1.0 / (1.0 + Math.Exp(-x)))));
        }

        static double SigmoidDerived(double x)
        {
            return Sigmoid(x) * (1 - Sigmoid(x));
        }
        static double SquaredError(double actual, double target)
        {
            return 0.5 * (target - actual) * (target - actual);
        }
        static double SquaredErrorDerived(double actual, double target)
        {
            //derived with respect to actual
            return -(target - actual);
        }

        static void PrintArray(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (i != array.Length - 1)
                    Console.Write(array[i].ToString() + ", ");
                else
                    Console.Write(array[i].ToString());
            }
            Console.WriteLine();
        }
    }
}
