﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleNN
{
    public partial class NNet
    {
        partial class Neuron
        {
            public partial class Synapse
            {
                /// <summary>
                /// Output node
                /// </summary>
                public Neuron forward;
                /// <summary>
                /// input node
                /// </summary>
                public Neuron back;
                /// <summary>
                /// weight of this synapse
                /// </summary>
                public double weight;
                /// <summary>
                /// Change in weight
                /// </summary>
                public double weightDelta;
            }

            /// <summary>
            /// Input synapse
            /// </summary>
            public List<Synapse> input = new List<Synapse>();
            /// <summary>
            /// output synapse
            /// </summary>
            public List<Synapse> output = new List<Synapse>();

            /// <summary>
            /// Activation Function (Input value from inputs = w0x0 + w1x1...wnxn + bias)
            /// </summary>
            public double Value;
            /// <summary>
            /// Input value from inputs = w0x0 + w1x1...wnxn + bias
            /// </summary>
            public double input_x;
            /// <summary>
            /// bias for this neuron
            /// </summary>
            public double Bias;
            /// <summary>
            /// DError/Doutput partial derivative of the output
            /// </summary>
            public double derrordoutput;
            /// <summary>
            /// partial derivative of the input 
            /// </summary>
            public double doutputdinput;

            /// <summary>
            /// Change in bias
            /// </summary>
            public double BiasDelta;

            public bool IsInputNeuron { get { return input.Count == 0; } }
            public bool IsOutputNeuron { get { return output.Count == 0; } }

            /// <summary>
            /// Forward propogation of this neuron based on parent neuron's value and synapse weights
            /// </summary>
            /// <param name="value"> If this is an input neuron, then this value will be used to set parameters</param>
            /// <param name="fx"> f(x), activation function </param>
            public void ForwardPropogate(Func<double,double> fx)
            {
                Value = fx(input_x = input.Sum(x => x.back.Value * x.weight) + Bias);
            }

            /// <summary>
            /// back Propogate this neuron to update all of the input synapses and weights
            /// </summary>
            /// <param name="dfdx"></param>
            public void BackPropogation(Func<double, double> dfdx, double learn_rate, double momentum)
            {
                //the purpose of back propogation is to update each weight by determining how
                //responsible each weight is for the total error. This can be represented as: dError/dWeight

                //to compute this we use partial derivatives and the chain rule
                //dError/dWeight = dError/doutput * doutput/dinput * dinput/dWeight
                //then we update the weight: Weight -= LearingRate * dError/dWeight

                //first compute dErrordOut
                derrordoutput = dErrordOutput();
                //compute doutputdinput
                doutputdinput = dfdx(input_x);
                //compute gradient
                double gradient = derrordoutput * doutputdinput;

                //iterate for each input (each input is from a different neuron)
                for (int i = 0; i < input.Count; i++)
                {
                    //dinput/dwieght is the partial derivative w/ respect to a specific wieght
                    //so the equation just reduces to the output of the connect synapse
                    double dinputdweight = input[i].back.Value;
                    double delta = learn_rate * gradient * dinputdweight;
                    //get previous weight delta
                    double prev_weight_delta = input[i].weightDelta;
                    //velocity = delta + momentum * prev_delta
                    double velocity = delta + prev_weight_delta * momentum;
                    //update weight
                    input[i].weight -= velocity;
                    //assign previous delta
                    input[i].weightDelta = velocity;
                }

                //update bias
                //get delta
                double b_delta = learn_rate * gradient;
                //get previous delta
                double prev_delta = BiasDelta;
                //velocity =  Bias delta + moment * previous delta
                double b_velocity = b_delta + prev_delta * momentum;
                Bias -= b_velocity;
                //assign delta
                BiasDelta = b_velocity;
            }

            /// <summary>
            /// Compute Error for this neuron
            /// </summary>
            double dErrordOutput()
            {
                //if not output neuron
                if (!IsOutputNeuron)
                {
                    //we take into account all of the (previous dError)/(current doutput)
                    double dedo = 0.0;
                    for (int i = 0; i < output.Count; i++)
                    {
                        //calculate output node gradient
                        double prev_derror_prev_dinput = output[i].forward.derrordoutput * output[i].forward.doutputdinput;
                        //get output synapse weight
                        double prev_dinput_current_doutput = output[i].weight;
                        //sum weight + gradient = error for this synapse
                        dedo += prev_derror_prev_dinput * prev_dinput_current_doutput;
                    }
                    //return sum of errors
                    return dedo;
                }
                else
                    return derrordoutput;
            }
        }
    }
}
