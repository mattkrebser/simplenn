﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleNN
{
    //TODO: 
    //****Gradient checking
    //Learning Rate annealing
    //Autoencoder wrapper
    //More Activation functions
    //More loss functions
    //CNN??

    /// <summary>
    /// Simple Neural Network class
    /// </summary>
    public partial class NNet
    {

        /// <summary>
        /// neural network layers
        /// </summary>
        List<List<Neuron>> Layers = new List<List<Neuron>>();

        double learningRate;
        double momentum;
        int epochs;
        bool debug_mode;
        int seed;

        /// <summary>
        /// activation function
        /// </summary>
        Func<double, double> fx;
        /// <summary>
        /// activation function derived
        /// </summary>
        Func<double, double> dfdx;
        /// <summary>
        /// Error function
        /// </summary>
        Func<double, double, double> eot;
        /// <summary>
        /// Error function derived
        /// </summary>
        Func<double, double, double> dedo;

        /// <summary>
        /// Neural Network constructor.
        /// </summary>
        /// <param name="layers"> layers describing the network. layers[0] is expected to be an input layer. 
        /// layers.Last is expected to be an output layer</param>
        /// <param name="learning_rate">learing rate </param>
        /// <param name="_momentum"> Helps converge. typical value = 0.9, Setting = 0, disables momentum </param>
        /// <param name="Epochs">number of training iterations</param>
        /// <param name="training_data">training input data</param>
        /// <param name="training_expected_output"> training data expected output</param>
        /// <param name="_fx">activation function</param>
        /// <param name="_dfdx">activation function derivative</param>
        /// <param name="_eot">Error Function. Computes the Error for the target value and the actual value. The left 
        /// input paramter is the actual value. The right input parameter is the target value.</param>
        /// <param name="_dedo">Error function derivative</param>
        /// <param name="debugMode">print debug messages?</param>
        /// <param name="seed">random node initializtion seed</param>
        public NNet(List<Layer> layers, double learning_rate, double _momentum,
            int Epochs, List<double[]> training_data, List<double[]> training_expected_output,
            Func<double, double> _fx, Func<double, double> _dfdx, Func<double, double, double> _eot,
            Func<double, double, double> _dedo, bool debugMode = true, int _seed = 0)
        {
            if (training_data == null || training_expected_output == null || training_data.Count == 0 ||
                training_expected_output.Count == 0)
                throw new System.Exception("Error invalid training data");
            if (layers.Count < 2 || layers == null)
                throw new System.Exception("Error, too few network layers. Must have atleast 2 layers");
            if (layers[0].LayerSize != training_data[0].Length)
                throw new System.Exception("First Layer(Input Layer) does not match the training data length!");
            if (layers[layers.Count - 1].LayerSize != training_expected_output[training_expected_output.Count - 1].Length)
                throw new System.Exception("Training expected out length doesn't match output length");
            if (training_expected_output.Count != training_data.Count)
                throw new System.Exception("Training data length mismatch");
            int input_length = training_data[0].Length;
            int output_length = training_expected_output[0].Length;
            if (training_data.Any(x => x.Length != input_length) || training_expected_output.Any(x => x.Length != output_length))
                throw new System.Exception("Training data length mismatch");

            //create a list of lists of neurons Each list of neurons represents a layer
            layers.ForEach(l => Layers.Add(Enumerable.Range(0, l.LayerSize).Select(x => new Neuron()).ToList()));
            learningRate = learning_rate;
            momentum = _momentum;
            epochs = Epochs;
            fx = _fx;
            dfdx = _dfdx;
            debug_mode = debugMode;
            eot = _eot;
            dedo = _dedo;
            seed = _seed;
            Initialize();
            train(training_data, training_expected_output);
        }

        /// <summary>
        /// Create synapses and initialize wieghts
        /// </summary>
        void Initialize()
        {
            Random r = new Random(seed);
            //create connections from current node to all possible next nodes
            for (int i = 0; i < Layers.Count - 1; i++)
            {
                var next = Layers[i + 1];
                var layer = Layers[i];
                for (int n = 0; n < layer.Count; n++)
                {
                    var from = layer[n];
                    for (int m = 0; m < next.Count; m++)
                    {
                        var to = next[m];
                        //make new synapse
                        var new_synapse = new Neuron.Synapse();
                        new_synapse.back = from;
                        new_synapse.forward = to;
                        //randomly initialize synapse weight
                        new_synapse.weight = (r.NextDouble() * 2.0 - 1.0);
                        //add as output of current layer and input to next layer
                        from.output.Add(new_synapse);
                        to.input.Add(new_synapse);
                    }
                }
            }
            //randomize bias values
            Layers.ForEach(layer => layer.ForEach(neuron => neuron.Bias = r.NextDouble() * 2 - 1));
        }

        void train(List<double[]> training_data, List<double[]> training_expected_output)
        {
            Random r = new Random(seed);
            //for number of epochs
            for (int i = 0; i < epochs; i++)
            {
                double error = 0.0;
                for (int j = 0; j < training_data.Count; j++)
                {
                    ForwardPropogate(training_data[j]);
                    if (debug_mode)
                        error += TotalError(training_expected_output[j]);
                    BackwardPropogate(training_expected_output[j]);
                }
                if (debug_mode)
                    Console.WriteLine("Total rate of Error: " + error / training_data.Count);
                //shuffle data, helps converge data faster and avoid cycles
                Shuffle(training_data, training_expected_output, r);
            }
        }

        void Shuffle(List<double[]> data, List<double[]> target, Random r)
        {
            for (int i = 0; i < data.Count; i++)
            {
                int n = r.Next(0, data.Count);
                var item = data[i];
                var t_item = target[i];
                data[i] = data[n];
                target[i] = target[n];
                data[n] = item;
                target[n] = t_item;
            }
        }

        void ForwardPropogate(double[] data)
        {
            //propogate first layer (just assigning input values)
            for (int i = 0; i < Layers[0].Count; i++)
                Layers[0][i].Value = data[i];

            //propogate layers
            //for each layer
            for (int i = 1; i < Layers.Count; i++)
            {
                //for each neuron in the layer
                var layer = Layers[i];
                for (int j = 0; j < layer.Count; j++)
                {
                    //propogate the neuron's data
                    layer[j].ForwardPropogate(fx);
                }
            }
        }

        /// <summary>
        /// Returns the total error of the network. Should be called after running a forward propogation
        /// </summary>
        /// <returns></returns>
        double TotalError(double[] target)
        {
            double sum = 0;
            var output = Layers[Layers.Count - 1];
            for (int i = 0; i < output.Count; i++)
            {
                sum += eot(output[i].Value, target[i]);
            }
            return sum;
        }

        void BackwardPropogate(double[] data)
        {
            //back propogate the last layer (we have to use the target values instead of previous error values)
            for (int i = 0; i < Layers[Layers.Count - 1].Count; i++)
                Layers[Layers.Count - 1][i].derrordoutput = dedo(Layers[Layers.Count - 1][i].Value, data[i]);

            //back propogation starts at the back of the network
            for (int i = Layers.Count - 1; i > 0; i--)
            {
                var layer = Layers[i];
                for (int j = 0; j < layer.Count; j++)
                {
                    //back propogate the neurons data
                    layer[j].BackPropogation(dfdx, learningRate, momentum);
                }
            }
        }

        /// <summary>
        /// Sample Input Data
        /// </summary>
        /// <param name="test_data"></param>
        /// <returns></returns>
        public double[] Sample(double[] test_data)
        {
            ForwardPropogate(test_data);
            double[] sample = new double[Layers[Layers.Count - 1].Count];
            for (int i = 0; i < sample.Length; i++)
            {
                sample[i] = Layers[Layers.Count - 1][i].Value;
            }
            return sample;
        }

        partial class Neuron
        {
            public partial class Synapse
            {

            }
        }

    }

    public class Layer
    {
        public int LayerSize { get; private set; }
        public Layer(int Size)
        {
            LayerSize = Size;
        }
    }
}
